import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'main',
      component: require('@/components/desktopApp/index').default,
      children: [{
        path: 'about',
        name: 'about',
        component: () => import('../components/desktopApp/view/about.vue')
      }]
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})